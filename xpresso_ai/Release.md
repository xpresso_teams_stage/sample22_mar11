# Release Note: Xpresso.AI

## Contains
1. What's new?
2. Major features
3. Bug fixes


## Release Summary ‐ What's new?
First alpha release of Xpresso.AI IDDME python package. It provides the necessary tools to create an Integrated Development, Deployment and Monitoring Environment (IDDME). This can be considered to be an alpha version of the xpresso.ai

 Bitbucket  Branch:  https://bitbucket.org/abzooba-screpo/xpresso.ai.git

## Major features
- XprLogger class
    - ELK Stack Setup
    - External XprLogger class
- Remote setup of Kubeadm, Kubernetes master node, Kubernetes worker node and kubernetes dashboard through Python scripts.
- Xpresso Command Line Interface (CLI)
- Xpresso Controller Library
- Xpresso API Server
- Xpresso Authentication Module
    - Login/Logout
    - Validation consisting of token generation
    -Token validation
    - Database and permission checks
- Xpresso User management
    - Register user : Creates a new user with login credentials to xprctl.
    - get users : retrieves the list of all the users in the database.
    - deactivate user : Deactivates a user from accessing the xprctl.
    - modify user : Modifies user information and status.
- Xpresso Node management (XAP - 277)
    - Register node : Creates a new node in the db if requested node is available.
    - get nodes : Retrieves the list of all the nodes in the db.
    - provision node : Provisions a node by installing necessary packages on the server.
    - deactivate node : Deactivates a node by deleting all the installed packages.
    - assign node : Assigns a development node to a user.
- Xpresso Project Management
    - Create project : Creates a skeleton project on bitbucket as per project information.  
    - Modify project : Update project components and information.
    - Get project : Retrieve the list of all the projects in the db.
    - Deactivate project : Deactivates a project from accessing it through xprctl.
- Xpresso Cluster Management
    - Methods to register, view and deactivate clusters.
- Xpresso Project Build
    - Scripts to facilitate automatic build of Xpresso projects and to view the build history.
- Xpresso Project Deployment
    - Scripts to Deploy/Undeploy projects onto a Kubernetes cluster via the Kubernetes Python API.
- Xpresso Bundle Manager
    - Perform Install, Uninstall, Start, Stop and Status for following packages
        - Base VM Bundle ( Include python, docker, nfs)
        - Python Development VM Bundle
        - Java Development  VM Bundle
        - Docker Distribution Bundle
        - Kubernetes Master/Node and dashboard package
        - Apt-Get repository Bundle
- Integration:
    - Bitbucket Integration
        - Connection to bitbucket to make changes to client projects. 
    - SSH client modules :
        - Connection to server through ssh client to run setup scripts.
    -Jenkins Integration
        - Remotely Trigger build in jenkins
        
## Bug fixes
Since this is a first release, there is no bugs.
